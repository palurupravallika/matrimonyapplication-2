package com.example.demo;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.Entity.Employee;
import com.example.demo.Repository.EmployeeRepository;

import jakarta.annotation.PostConstruct;

@SpringBootApplication
public class EmployeeManagementApplication {

//	@Autowired
//	private EmployeeRepository employeerespository;
//
//	@PostConstruct
//
//	public void init() {
//		employeerespository
//				.saveAll(Stream
//						.of(new Employee(1, "Pravallika", 20000, "pravallikapaluri@gmail.com"),
//								new Employee(2, "Sravani", 30000, "sravanip12@gmail.com"))
//						.collect(Collectors.toList()));
//	}

	public static void main(String[] args) {
		SpringApplication.run(EmployeeManagementApplication.class, args);
	}

}
