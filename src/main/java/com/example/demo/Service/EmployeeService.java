package com.example.demo.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.Entity.Employee;

@Service
public interface EmployeeService {

	List<Employee> getAllEmployees();

	Employee getEmployeeById(int empId);

	Employee addEmployee(Employee employee);

	Employee updateEmployee(int empId, Employee employee);

	void deleteEmployee(int empId);

}
