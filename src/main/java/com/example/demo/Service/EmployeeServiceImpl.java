package com.example.demo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Employee;
import com.example.demo.Repository.EmployeeRepository;

@Service

public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	public EmployeeRepository getEmployeerepository() {
		return employeeRepository;
	}

	public void setEmployeerepository(EmployeeRepository employeerepository) {
		this.employeeRepository = employeerepository;
	}

@Override
	public List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public Employee getEmployeeById(int empId) {
		return employeeRepository.findById(empId).orElse(null);
	}

	@Override
	public Employee addEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	@Override
	public Employee updateEmployee(int empId, Employee employee) {

		return employeeRepository.save(employee);

	}

	@Override
	public void deleteEmployee(int empId) {
		employeeRepository.deleteById(empId);
	}
}
